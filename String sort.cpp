#include <iostream>
#include <fstream>
#include <string>
#include <deque>
#include <algorithm>
#include <chrono>
#include <vector>
#include "windows.h"

using namespace std;
using namespace std::chrono;

void sort_func(bool& exit);
void write_data(const string& inputFileName, const deque<char>& outputDq);
string make_new_file_path(const string& orgFspec);

int main()
{
    bool exiting = false;

    do
    {
        try
        {
            sort_func(exiting);
        }
        catch (const exception& e)
        {
            cout << "Error encountered: ";
            cout << e.what();

            Sleep(3000);
            return(0);
        }

    } while (!exiting);

    return(0);
}

void sort_func(bool& exit)
{
    char resp = 'N';
    deque<char> charDq;
    string userInput;
    ifstream inputFile;

    const auto is_valid_char = [](const char& c) {return(c > 0x20 && c <= 0x7f); };

    const auto sort_chars = [](char& a, char& b) {return(a < b); };

    const auto valid_resp = [](const char& c) {return(c == 'Y' || c == 'y' || c == 'N' || c == 'n'); };

    const auto display_data = [&](const milliseconds& duration)
    {
        for (const auto& c : charDq)
            cout << c;

        cout << endl;

        cout << "Sort complete - Time taken: " << duration.count() << " milliseconds" << endl;
    };

    cout << "Enter a string that you wish to sort.\r\n\r\n";
    cout << "Alternatively you may sort the contents of a file by entering 'file=' followed by the file path." << endl;

    while (userInput.empty())
        getline(cin, userInput);

    if (userInput.find("file=") != string::npos) //Check if a file path has been entered.
    {
        const auto filePath = userInput.substr(5);

        const auto startTime = high_resolution_clock::now();

        inputFile.open(filePath);

        if (!inputFile.good())
            throw exception("Failed to open the specified file.");

        while (!inputFile.eof()) //Read through to end of file
        {
            char c;
            inputFile.get(c);

            if (is_valid_char(c))
                charDq.push_back(c);
        }

        inputFile.close();

        if (charDq.empty())
            throw exception("The specified file is empty.");

        sort(begin(charDq), end(charDq), sort_chars);

        const auto stopTime = high_resolution_clock::now();

        display_data(duration_cast<milliseconds>(stopTime - startTime));

        write_data(filePath, charDq);
    }
    else // User has entered a string.
    {
        for (int i = 0; i < userInput.size(); i++)
        {
            const auto c = userInput.at(i);

            if (is_valid_char(c))
                charDq.push_back(c);
        }

        const auto startTime = high_resolution_clock::now();

        sort(begin(charDq), end(charDq), sort_chars);

        const auto stopTime = high_resolution_clock::now();

        display_data(duration_cast<milliseconds>(stopTime - startTime));
    }

    cout << "Would you like to sort another string? [Y/N]\r\n";
    resp = cin.get();

    if (!valid_resp(resp))
        throw exception("Invalid response.");

    if (resp == 'N' || resp == 'n')
        exit = true;
}

string make_new_file_path(const string& orgFspec)
{
    string outputFile = orgFspec; //Take a copy of the input file name

    size_t startExt = outputFile.find_last_of("."); //Sub the current extension
    outputFile = outputFile.substr(0, startExt);

    outputFile += "_sorted_";
    long counter = 1;

    const auto file_exists = [&]() //Find the next unique file name
    {
        string testName = outputFile;
        testName += to_string(counter);
        testName += ".txt";

        ifstream f(testName);
        return f.good();
    };

    while (file_exists())
        counter++;

    outputFile += to_string(counter) += ".txt";

    return(outputFile);
}

void write_data(const string& inputFileName, const deque<char>& outputDq)
{
    const auto outputFileName = make_new_file_path(inputFileName);

    //Open file 
    ofstream outputFile(outputFileName);

    const auto startTime = high_resolution_clock::now();

    if (!outputFile.is_open())
        throw exception("Failed to open output file.");

    for (const auto& c : outputDq)
        outputFile << c;

    outputFile.close();

    const auto stopTime = high_resolution_clock::now();

    const milliseconds duration = duration_cast<milliseconds>(stopTime - startTime);

    cout << "New file created - " << outputFileName << " Written in " << duration.count() << " milliseconds. Size = " << outputDq.size() << " bytes." << endl;
};





